package com.springboot.mvc.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springboot.mvc.entity.Empleado;

@Repository
public interface EmpleadoDao  extends JpaRepository<Empleado, Long>{

}
