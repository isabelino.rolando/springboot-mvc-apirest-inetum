package com.springboot.mvc.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.springboot.mvc.entity.Empleado;
import com.springboot.mvc.service.EmpleadoService;

@Controller
public class EmpleadoController {

	/*@GetMapping("empleados")
	public String hola() {
		return "empleado";
	}*/
	
	@Autowired
	private EmpleadoService servicio;
	
	@GetMapping({"/empleados","/"})
	public String index(Model modelo) {
		
		modelo.addAttribute( "key",servicio.listarEmpleados() );
		
		return "empleado";
	}
	
	//metodos para registrar
	@GetMapping("/empleado/nuevo")
	public String nuevoEmpleadoRegistro(Model modelo) {
		Empleado empleado = new Empleado();
		modelo.addAttribute("emp",empleado);
		return "nuevo_empleado";
	}
	
	@PostMapping("/empleado")
	public String guardarEmpleado(@ModelAttribute("emp") Empleado empleado) {
		servicio.guardarEmpleado(empleado);
		return "redirect:/empleados";
	}
	
	//metodos para actualizar
	
	@GetMapping("/empleado/editar/{id}")
	public String editarEmpleadoForm(@PathVariable Long id, Model modelo) {
		Empleado empleadoEdit =servicio.obtenerEmpleadoPorId(id);
		modelo.addAttribute("empEdit",empleadoEdit);
		return "editar_empleado";
	}
	
	@PostMapping("/empleado/{id}")
	public String editarEmpleado( @PathVariable Long id,@ModelAttribute("empEdit") Empleado empleado) {
		
		Empleado empleadoEditar= servicio.obtenerEmpleadoPorId(id);
		empleadoEditar.setNombre(empleado.getNombre());
		empleadoEditar.setApellido(empleado.getApellido());
		empleadoEditar.setEmail(empleado.getEmail());
		empleadoEditar.setTelefono(empleado.getTelefono());
		
		servicio.guardarEmpleado(empleadoEditar);
		
		return "redirect:/empleados";
	}
	
	
	@PostMapping("/empleado/eliminar/{id}")
	public String eliminarEmpleado(@PathVariable("id") Long id) {
		servicio.eliminarEmpleado(id);
		return "redirect:/empleados";
	}
	
	
	
	
}
